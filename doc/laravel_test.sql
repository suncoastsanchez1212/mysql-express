-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 03, 2020 at 04:17 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `apps`
--

DROP TABLE IF EXISTS `apps`;
CREATE TABLE IF NOT EXISTS `apps` (
  `app_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `api_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redmine_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `apps`
--

INSERT INTO `apps` (`app_id`, `name`, `api_url`, `description`, `redmine_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '카드보고서 1', 'public/image/20190604015051.png', '카드보고서', '1002', '1', '2019-06-03 18:50:51', '2019-06-03 18:50:51'),
(2, '카드보고서 2', 'public/image/20190604020704.jpg', NULL, '1002', '1', '2019-06-03 19:07:04', '2019-06-03 19:07:04'),
(3, '카드보고서 3', 'public/image/20190604084129.png', 'http://www.legend.com.kh/', '1003', '1', '2019-06-04 01:41:29', '2019-06-04 01:41:29'),
(4, '카드보고서 4', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(5, '카드보고서 5', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(6, '카드보고서 6', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(7, '카드보고서 6', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(8, '카드보고서 6', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(9, '카드보고서 6', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(10, '카드보고서 6', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(11, '카드보고서 6', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(12, '카드보고서 6', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(13, '카드보고서 6', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(14, '카드보고서 6', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(15, '카드보고서 6', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(16, '카드보고서 6', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(17, '카드보고서 6', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(18, '카드보고서 6', '카드보고서 URL', '카드보고서 Description', '1002', '1', NULL, NULL),
(19, '카드보고서 10', '카드보고서 10 URL', '카드보고서 10 Description', '1002', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('unit','measure') COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(13, '2014_10_12_000000_create_users_table', 1),
(14, '2014_10_12_100000_create_password_resets_table', 1),
(15, '2019_04_03_072742_create_categories_table', 1),
(16, '2019_06_03_014800_create_apps_table', 1),
(17, '2019_06_03_075930_create_sub_apps_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_apps`
--

DROP TABLE IF EXISTS `sub_apps`;
CREATE TABLE IF NOT EXISTS `sub_apps` (
  `subapp_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `app_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subapp_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_subapp` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`subapp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_apps`
--

INSERT INTO `sub_apps` (`subapp_id`, `app_id`, `subapp_name`, `link_url`, `order_subapp`, `created_at`, `updated_at`) VALUES
(1, '1', 'Coca cola', 'cocacola', 1, '2019-06-03 19:24:21', '2019-06-03 19:24:21'),
(2, '1', 'Hello App Pokosor', 'http://www.legend.com.kh/', 1, '2019-06-04 01:41:11', '2019-06-04 01:41:11'),
(3, '3', 'Movies Type', 'url of movies....', 1, '2019-06-04 01:43:32', '2019-06-04 01:43:32'),
(4, '3', 'Player url', 'player url...', 1, '2019-06-04 01:43:50', '2019-06-04 01:43:50'),
(5, '3', 'api irl', 'api url', 1, '2019-06-04 01:47:40', '2019-06-04 01:47:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `email`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'thatt thonn', '$2y$10$VHmvq4avQAmMVRRrqfgWZOISgQLBFb4ONAUNafqH//tDTGFuZs6L2', 'that.thon24@gmail.com', '2', NULL, '2019-06-03 18:50:28', '2019-06-03 18:50:28');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
