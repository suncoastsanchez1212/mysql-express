var http = require("http");
var express = require("express");
const bodyParser = require("body-parser");
var mysqlcon = require("./database/mysql");

var swaggerJsdoc = require("swagger-jsdoc");
var swaggerUi = require("swagger-ui-express");
var request = require('request');

var hostname = "127.0.0.1";
var port = 3000;
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.raw());

// create a server runnung
var server = app.listen(port, hostname, function(req, res) {
  console.log(`Server running at http://${hostname}:${port}/`);
});

// swagger configuration
// Swagger set up
const options = {
  swaggerDefinition: {
    openapi: "3.0.0",
    info: {
      title: "Time to document that Express API you built",
      version: "1.0.0",
      description:
        "A test project to understand how easy it is to document and Express API",
      license: {
        name: "MIT",
        url: "https://choosealicense.com/licenses/mit/"
      },
      contact: {
        name: "Swagger",
        url: "https://swagger.io",
        email: "Info@SmartBear.com"
      }
    },
    servers: [
      {
        url: "http://localhost:3000"
      }
    ]
  },
  apis: ["app.js"]
};
const swaggerDoc = swaggerJsdoc(options);
app.use("/docs", swaggerUi.serve);
app.get(
  "/docs",
  swaggerUi.setup(swaggerDoc, {
    explorer: true
  })
);

// create pool connection
/**
 * @swagger
 *  /:
 *  get:
 *      tags:
 *          - MySQL Pool
 *      summary: request all apps.
 *      responses:
 *          '200':
 *              description: agreed
 */
app.get("/", function(req, res) {
  var arr = [];
  mysqlcon.get_connection_pool().getConnection(function(err, conn) {
    if (err) throw err;
    conn.query("SELECT * FROM apps", function(err, result, fields) {
      if (err) throw err;
      for (var index in result) {
        if (result.hasOwnProperty(index)) {
          var element = result[index];
          arr.push(element);
        }
      }
      conn.release();
      res.statusCode = 200;
      res.json({ data: result, message: "okay", code: "0000" });
      //   res.json({ data: arr, message: "okay", code: "0000" });
    });
  });
});

// json format for post method
/*{
    "name": "카드보고서 10",
    "api_url": "카드보고서 10 URL",
    "description": "카드보고서 10 Description",
    "redmine_id": "1002",
    "user_id": "1"
  }*/
/**
 * @swagger
 * /:
 *   post:
 *     tags:
 *       - MySQL Pool
 *     summary: Create an new app
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *             api_url:
 *               type: string
 *             description:
 *               type: string
 *             redmine_id:
 *               type: string
 *             user_id:
 *               type: string
 *         required:
 *           - name
 *           - api_url
 *           - redmine_id
 *           - user_id
 *     responses:
 *       200:
 *         description: Apps createdsuccessfully
 *       401:
 *         description: Bad data, not found in db
 *       403:
 *         description: Username and password don't match
 */
app.post("/", function(req, res) {
  console.log('postman values:  ',req.body);
  mysqlcon.get_connection_pool().getConnection(function(err, conn) {
    if (err) throw err;
    var str_sql = `INSERT INTO apps(name, api_url, description, redmine_id, user_id) VALUES('${req.body.name}', '${req.body.api_url}', '${req.body.description}', '${req.body.redmine_id}', '${req.body.user_id}')`;
    console.log('sql, ',str_sql);
    conn.query(
      str_sql,
      function(err, result) {
        if (err) throw err;
        conn.release();
        res.statusCode = 200;
        res.json({ data: result.affectedRows, message: "okay", code: "0000" });
      }
    );
  });
});

// mysql connection
/**
 * @swagger
 *  /apps:
 *  get:
 *      tags:
 *          - MySQL Creation
 *      summary: request all apps.
 *      responses:
 *          '200':
 *              description: agreed
 */
app.get("/apps", function(req, res) {
  var arr = [];
  mysqlcon
    .connectdb()
    .query("SELECT * FROM apps", function(err, result, fields) {
      if (err) throw err;
      for (var index in result) {
        if (result.hasOwnProperty(index)) {
          var element = result[index];
          arr.push(element);
        }
      }
      res.statusCode = 200;
      res.json({ data: arr, message: "okay", code: "0000" });
    });
});

/**
 * @swagger
 *  /apps:
 *  post:
 *      summary: Creates a new app.
 *      consumes:
 *          -application/json
 *      tags:
 *          - MySQL Creation
 *      parameters:
 *          -in: body
 *          name: app
 *          description: The user to create.
 *          schema:
 *              type: object
 *              required:
 *                   - userName
 *              properties:
 *                   userName:
 *                      type: string
 *              firstName:
 *                      type: string
 *              lastName:
 *                       type: string
 *      responses:
 *          '201':
 *              description: agreed
 */
app.post("/apps", function(req, res) {
  const arr = [];
  mysqlcon
    .connectdb()
    .query(
      "INSERT INTO apps(name, api_url, description, redmine_id, user_id) VALUES('카드보고서 6', '카드보고서 URL', '카드보고서 Description', '1002', '1')",
      function(err, result) {
        if (err) throw err;
        res.statusCode = 200;
        res.json({ data: arr, message: "okay", code: "0000" });
      }
    );
});
